import { Component } from "react";
import axios from "axios";

class Pizza365axiosApi extends Component {
    axiosApi = async (url, body) => {
        const response = await axios(url, body);
        return response.data;
    }

    //function get all order
    getAllBtnHandler = () => {
        this.axiosApi("http://203.171.20.210:8080/devcamp-pizza365/orders")
            .then((data) => {
                console.log("Get all order");
                console.log(data)
            })
            .catch((error) => {
                console.log(error.message)
            })

    }
    // Create new order
    createBtnHandler = () => {
        const body = {
            method: "POST",
            data: JSON.stringify({
                kichCo: "M",
                duongKinh: "25",
                suon: "4",
                salad: "300",
                loaiPizza: "HAWAII",
                idVourcher: "16512",
                idLoaiNuocUong: "PEPSI",
                soLuongNuoc: "3",
                hoTen: "Phạm Thanh Bình",
                thanhTien: "200000",
                email: "binhpt001@devcamp.edu.vn",
                soDienThoai: "0865241654",
                diaChi: "Hà Nội",
                loiNhan: "Pizza đế dày"
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }
        console.log(body)
        this.axiosApi("http://203.171.20.210:8080/devcamp-pizza365/orders", body)
            .then((data) => {
                console.log("Create new order");
                console.log(data)
            })
            .catch((error) => {
                console.log(error.message)
            })
    }
    // Get order by id
    getOrderByIdBtnHandler = () => {
        const orderId = "f7X4uuQoeN"
        this.axiosApi("http://203.171.20.210:8080/devcamp-pizza365/orders/" + orderId)
            .then((data) => {
                console.log("Get order by id");
                console.log(data)
            })
            .catch((error) => {
                console.log(error.message)
            })
    }
    // Update order by id
    updateBtnHandler = () => {
        const orderId = "78081"
        const body = {
            method: "PUT",
            data: JSON.stringify({
                trangThai: "Cancel"
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }
        this.axiosApi("http://203.171.20.210:8080/devcamp-pizza365/orders/" + orderId, body)
            .then((data) => {
                console.log("Update order by id");
                console.log(data)
            })
            .catch((error) => {
                console.log(error.message)
            })
    }
    // Check voucher
    checkVoucherBtnHandler = () => {
        const voucherId = "16512"
        this.axiosApi("http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + voucherId)
            .then((data) => {
                console.log("Check voucher");
                console.log(data)
            })
            .catch((error) => {
                console.log(error.message)
            })
    }
    // Get Drink List
    getDrinkBtnHandler = () => {
        this.axiosApi("http://203.171.20.210:8080/devcamp-pizza365/drinks")
            .then((data) => {
                console.log("Get all drink");
                console.log(data)
            })
            .catch((error) => {
                console.log(error.message)
            })
    }
    render() {
        return (
            <div>
                <div className="mt-5">
                    <p>
                        Test Page for Javascript Tasks. F5 to run code
                    </p>
                </div>
                <div className="row form-group">
                    <div className="div-button">
                        <button className="btn btn-primary m-1" onClick={this.getAllBtnHandler}>Call api get all orders</button>
                    </div>
                    <div className="div-button">
                        <button className="btn btn-info m-1" onClick={this.createBtnHandler}>Call api create order</button>
                    </div>
                    <div className="div-button">
                        <button className="btn btn-success m-1" onClick={this.getOrderByIdBtnHandler}>Call api get order by id</button>
                    </div>
                    <div className="div-button">
                        <button className="btn btn-secondary m-1" onClick={this.updateBtnHandler}>Call api update order</button>
                    </div>
                    <div className="div-button">
                        <button className="btn btn-danger m-1" onClick={this.checkVoucherBtnHandler}>Call api check voucher by id</button>
                    </div>
                    <div className="div-button">
                        <button className="btn btn-success m-1" onClick={this.getDrinkBtnHandler}>Call api get drink list</button>
                    </div>
                </div>
                <div className="mt-5">
                    <p><i>Demo 6 API for Pizza 365 project:</i></p>
                    <li>Get all orders: lấy tất cả orders</li>
                    <li>Create order: tạo 1 order mới</li>
                    <li>Get order by ID: lấy 1 order bằng ID</li>
                    <li>Update order: update 1 order bằng ID</li>
                    <li>Check voucher by ID: check thông tin mã giảm giá, quan trọng là có hay không và % giảm giá</li>
                    <li>Get drink list: lấy danh sách đồ uống</li>
                </div>
                <div className="mt-3">
                    <p className="text-danger"><b>Bật console log để nhìn rõ output</b></p>
                </div>
            </div>
        )
    }
}
export default Pizza365axiosApi;