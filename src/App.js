import "bootstrap/dist/css/bootstrap.min.css"

import Pizza365axiosApi from "./components/Pizza365RestSampleApi";

import './App.css';

function App() {
  return (
    <div className="container bg-light">
      <Pizza365axiosApi/>
    </div>
  );
}

export default App;
